<!-- @dd-navbar -->
<!-- this line automatically maintained by update-navbars --><nav style="text-align: right; margin-bottom: 12px;">[ <em>docs: <a href="https://docs.rs/derive-deftly/latest/derive_deftly/index.html">crate top-level</a> | <a href="https://docs.rs/derive-deftly/latest/derive_deftly/index.html#overall-toc">overall toc, macros</a> | <a href="https://docs.rs/derive-deftly/latest/derive_deftly/doc_reference/index.html">template etc. reference</a> | <a href="https://diziet.pages.torproject.net/rust-derive-deftly/latest/guide/"><strong>guide/tutorial</strong></a></em> ]</nav>

# What you should know before you start

Before you start using `derive-deftly`,
there are a few things t2hat this guide assumes
you are already familiar with.
Don't feel bad if you don't know this stuff already;
just make a note to come back later
once you're ready.

* We assume that you are already familiar with
  the Rust programming language
  and the Cargo package management tool.
  If you're not,
  maybe try the [Rust Book]
  or [_Rust By Example_].

* We assume that you are comfortable writing,
  building, compiling, and debugging Rust programs.

* We assume that you are already familiar
  with the `macro_rules!` syntax
  for defining "macros by example".
  If you're not,
  see the [relevant][book-macros] [sections][ex-macros]
  in the above books.

If these assumptions are true for you,
then let's move ahead!

[Rust Book]: https://doc.rust-lang.org/book/
[_Rust By Example_]: https://doc.rust-lang.org/rust-by-example/
[book-macros]: https://doc.rust-lang.org/book/ch19-06-macros.html
[ex-macros]: https://doc.rust-lang.org/rust-by-example/macros.html
